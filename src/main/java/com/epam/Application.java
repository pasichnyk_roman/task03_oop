package com.epam;

import com.epam.controller.impl.PlaneControllerImpl;
import com.epam.view.MyView;

public class Application {

    public static void main(String[] args) throws Exception{

        PlaneControllerImpl planeController = new PlaneControllerImpl();

        MyView myView = new MyView();
        myView.show();

    }

}
