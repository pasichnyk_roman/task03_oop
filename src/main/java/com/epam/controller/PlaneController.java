package com.epam.controller;

import com.epam.model.domains.BasicPlaneDomain;
import com.epam.model.domains.HeavyPlaneDomain;

import java.util.List;

public interface PlaneController {

    void addPlane() throws Exception;

    void printAllPlanes();

    int maxAllWeight();

    int maxAllPeople();

    void findPlaneByFuel();

    void sortPlaneByFlightRange();

}
