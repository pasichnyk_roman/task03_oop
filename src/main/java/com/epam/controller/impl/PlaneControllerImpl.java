package com.epam.controller.impl;

import com.epam.controller.PlaneController;
import com.epam.model.CivilPlaneBusinessLogic;
import com.epam.model.HeavyPlaneBusinessLogic;
import com.epam.model.domains.BasicPlaneDomain;
import com.epam.model.enums.PlaneType;
import edu.emory.mathcs.backport.java.util.Collections;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

public class PlaneControllerImpl implements PlaneController {

    private static Scanner sc = new Scanner(System.in);
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    private CivilPlaneBusinessLogic civilPlaneBusinessLogic = new CivilPlaneBusinessLogic();
    private HeavyPlaneBusinessLogic heavyPlaneBusinessLogic = new HeavyPlaneBusinessLogic();

    private List<BasicPlaneDomain> planes = new ArrayList<>();


    @Override
    public void addPlane() throws Exception {

        System.out.println("Enter the plane name");
        String planeName = br.readLine();

        System.out.println("Enter the max weight");
        while (!sc.hasNextInt()) {
            System.out.println("Enter the max weight");
            sc.next();
        }
        int maxWeight = sc.nextInt();

        System.out.println("Enter the max amount of people");
        while (!sc.hasNextInt()) {
            System.out.println("Enter the max weight");
            sc.next();
        }
        int maxPeople = sc.nextInt();

        System.out.println("Enter the flight range");
        while (!sc.hasNextInt()) {
            System.out.println("Enter the flight range");
            sc.next();
        }
        int flightRange = sc.nextInt();

        System.out.println("Enter the Fuel Consumption");
        while (!sc.hasNextFloat()) {
            System.out.println("Enter the Fuel Consumption");
            sc.next();
        }
        float fuelConsumption = sc.nextFloat();

        System.out.println("Select plane type: 1 - Heavy and 2 - Civil");
        while (!sc.hasNextByte()) {
            System.out.println("Select plane type: 1 - Heavy and 2 - Civil");
            sc.next();
        }
        byte type = sc.nextByte();
        boolean cargoCompartment;
        if (type == 1) {
            System.out.println("Enter list of guns");
            String guns = br.readLine();
            planes.add(heavyPlaneBusinessLogic.addPlane(planeName, maxWeight, maxPeople, PlaneType.HEAVY, fuelConsumption, flightRange, guns));

        } else {
            String answer;
            do {
                System.out.println("Specify available cargo compartment - Yes or No");
                answer = sc.next().toUpperCase();
            } while (!answer.equals("YES") && !answer.equals("NO"));
            cargoCompartment = answer.equals("YES");
            planes.add(civilPlaneBusinessLogic.addPlane(planeName, maxWeight, maxPeople, PlaneType.CIVIL, fuelConsumption, flightRange, cargoCompartment));
        }

    }

    @Override
    public void printAllPlanes() {
        for (BasicPlaneDomain plane : planes) {
            System.out.println(plane);
        }
    }

    @Override
    public int maxAllWeight() {
        int maxAllWeight = 0;
        for (BasicPlaneDomain plane : planes) {
            maxAllWeight += plane.getMaxWeight();
        }
        return maxAllWeight;
    }

    @Override
    public int maxAllPeople() {
        int maxAllPeople = 0;
        for (BasicPlaneDomain plane : planes) {
            maxAllPeople += plane.getMaxPeople();
        }
        return maxAllPeople;
    }

    @Override
    public void findPlaneByFuel() {
        List<BasicPlaneDomain> planesByFuel = new ArrayList<>();

        System.out.println("Enter the range [a] of Fuel Consumption");
        while (!sc.hasNextFloat()) {
            System.out.println("Enter the range [a] of Fuel Consumption");
            sc.next();
        }
        float fuelConsumptionStart = sc.nextFloat();
        System.out.println("Enter the range [b] of Fuel Consumption");
        while (!sc.hasNextFloat()) {
            System.out.println("Enter the range [b] of Fuel Consumption");
            sc.next();
        }
        float fuelConsumptionEnd = sc.nextFloat();

        if (fuelConsumptionStart > fuelConsumptionEnd) {
            float buf;
            buf = fuelConsumptionEnd;
            fuelConsumptionEnd = fuelConsumptionStart;
            fuelConsumptionStart = buf;
        }

        for (BasicPlaneDomain plane : planes) {
            if (plane.getFuelConsumption() >= fuelConsumptionStart && plane.getFuelConsumption() <= fuelConsumptionEnd) {
                planesByFuel.add(plane);
            }
        }
        System.out.println("Only planes which have fuel consumption in the range between " + fuelConsumptionStart + " and " + fuelConsumptionEnd + ":");
        for (BasicPlaneDomain plane : planesByFuel) {
            System.out.println(plane);
        }

    }

    @Override
    public void sortPlaneByFlightRange() {
        planes.sort((a, b) -> Integer.compare(a.getFlightRange(), b.getFlightRange()));
        for (BasicPlaneDomain plane : planes) {
            System.out.println(plane);
        }
    }
}
