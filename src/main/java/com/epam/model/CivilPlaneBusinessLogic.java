package com.epam.model;

import com.epam.model.domains.CivilPlaneDomain;
import com.epam.model.enums.PlaneType;

import java.util.Scanner;

public class CivilPlaneBusinessLogic implements CivilPlaneModel {

    private CivilPlaneDomain civilPlaneDomain;

    private static Scanner sc = new Scanner(System.in);

    public CivilPlaneBusinessLogic() {

    }

    @Override
    public CivilPlaneDomain addPlane(String name, int maxWeight, int maxPeople, PlaneType planeType, float fuelConsumption, int flightRange, boolean cargoCompartment) {
        civilPlaneDomain = new CivilPlaneDomain(name, maxWeight, maxPeople, planeType, fuelConsumption, flightRange, cargoCompartment);
        return civilPlaneDomain;
    }


    @Override
    public CivilPlaneDomain printPlane() {
        return civilPlaneDomain;
    }
}
