package com.epam.model;

import com.epam.model.domains.CivilPlaneDomain;
import com.epam.model.enums.PlaneType;

public interface CivilPlaneModel {


    CivilPlaneDomain addPlane(String name, int maxWeight, int maxPeople, PlaneType planeType, float fuelConsumption, int flightRange, boolean cargoCompartment);

    CivilPlaneDomain printPlane();





}
