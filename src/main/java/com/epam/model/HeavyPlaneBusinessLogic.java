package com.epam.model;

import com.epam.model.domains.HeavyPlaneDomain;
import com.epam.model.enums.PlaneType;

public class HeavyPlaneBusinessLogic implements HeavyPlaneModel {

    HeavyPlaneDomain heavyPlaneDomain;

    @Override
    public HeavyPlaneDomain addPlane(String name, int maxWeight, int maxPeople, PlaneType planeType, float fuelConsumption, int flightRange, String guns) {
        heavyPlaneDomain = new HeavyPlaneDomain(name, maxWeight, maxPeople, planeType, fuelConsumption, flightRange, guns);
        return heavyPlaneDomain;
    }

    @Override
    public HeavyPlaneDomain printPlane() {
        return heavyPlaneDomain;
    }
}
