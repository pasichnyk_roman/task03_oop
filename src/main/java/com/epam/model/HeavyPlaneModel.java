package com.epam.model;

import com.epam.model.domains.HeavyPlaneDomain;
import com.epam.model.enums.PlaneType;

public interface HeavyPlaneModel {

    HeavyPlaneDomain addPlane(String name, int maxWeight, int maxPeople, PlaneType planeType, float fuelConsumption, int flightRange, String guns);

    HeavyPlaneDomain printPlane();

}
