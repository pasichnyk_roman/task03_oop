package com.epam.model.domains;

import com.epam.model.enums.PlaneType;

public class BasicPlaneDomain {

    private String name;

    private int maxWeight;

    private int maxPeople;

    private PlaneType planeType;

    private float fuelConsumption;

    private int flightRange;

    public BasicPlaneDomain() {
    }

    public BasicPlaneDomain(String name, int maxWeight, int maxPeople, PlaneType planeType, float fuelConsumption, int flightRange) {
        this.name = name;
        this.maxWeight = maxWeight;
        this.maxPeople = maxPeople;
        this.planeType = planeType;
        this.fuelConsumption = fuelConsumption;
        this.flightRange = flightRange;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public int getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(int maxPeople) {
        this.maxPeople = maxPeople;
    }

    public PlaneType getPlaneType() {
        return planeType;
    }

    public void setPlaneType(PlaneType planeType) {
        this.planeType = planeType;
    }

    public float getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(float fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public void setFlightRange(int flightRange) {
        this.flightRange = flightRange;
    }

    @Override
    public String toString() {
        return "BasicPlaneDomain{" +
                "name='" + name + '\'' +
                ", maxWeight=" + maxWeight +
                ", maxPeople=" + maxPeople +
                ", planeType=" + planeType +
                ", fuelConsumption=" + fuelConsumption +
                ", flightRange=" + flightRange +
                '}';
    }
}
