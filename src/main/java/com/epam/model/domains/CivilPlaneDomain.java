package com.epam.model.domains;

import com.epam.model.enums.PlaneType;

public class CivilPlaneDomain extends BasicPlaneDomain {

    private boolean cargoCompartment;

    public CivilPlaneDomain() {
    }

    public CivilPlaneDomain(String name, int maxWeight, int maxPeople, PlaneType planeType, float fuelConsumption, int flightRange, boolean cargoCompartment) {
        super(name, maxWeight, maxPeople, planeType, fuelConsumption, flightRange);
        this.cargoCompartment = cargoCompartment;
    }

    public boolean isCargoCompartment() {
        return cargoCompartment;
    }

    public void setCargoCompartment(boolean cargoCompartment) {
        this.cargoCompartment = cargoCompartment;
    }

    public String fly() {
        return "Fly by Civil Plane";
    }

    @Override
    public String toString() {
        return "CivilPlaneDomain{" +
                super.toString() +
                ", cargoCompartment=" + cargoCompartment +
                '}';
    }
}
