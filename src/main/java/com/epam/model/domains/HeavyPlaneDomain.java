package com.epam.model.domains;

import com.epam.model.enums.PlaneType;

import java.util.List;

public class HeavyPlaneDomain extends BasicPlaneDomain {

    private String guns;

    public HeavyPlaneDomain() {
    }

    public HeavyPlaneDomain(String name, int maxWeight, int maxPeople, PlaneType planeType, float fuelConsumption, int flightRange, String guns) {
        super(name, maxWeight, maxPeople, planeType, fuelConsumption, flightRange);
        this.guns = guns;
    }

    public String getGuns() {
        return guns;
    }

    public void setGuns(String guns) {
        this.guns = guns;
    }

    public String fly() {
        return "Fly by Heavy Plane";
    }

    @Override
    public String toString() {
        return "HeavyPlaneDomain{" +
                super.toString() +
                ", guns=" + guns +
                '}';
    }
}
