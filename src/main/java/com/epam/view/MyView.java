package com.epam.view;

import com.epam.controller.impl.PlaneControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private PlaneControllerImpl planeController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() throws Exception {
        planeController = new PlaneControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - add Plane");
        menu.put("2", "  2 - print all planes");
        menu.put("3", "  3 - print max weight in all planes together");
        menu.put("4", "  4 - print max amount of people in all planes together");
        menu.put("5", "  5 - find planes by fuel consumption");
        menu.put("6", "  6 - sort planes by flight range");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() throws Exception {
        planeController.addPlane();
    }

    private void pressButton2() {
        planeController.printAllPlanes();
    }

    private void pressButton3() {
        System.out.println("Max Weight: " + planeController.maxAllWeight());
    }

    private void pressButton4() {
        System.out.println("Max amount of people: " + planeController.maxAllPeople());
    }

    private void pressButton5() {
        planeController.findPlaneByFuel();
    }

    private void pressButton6() {
        planeController.sortPlaneByFlightRange();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
